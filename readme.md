```mermaid
graph TD
    A[Veuillez scanner votre badge] -->|Scan| B{Utilisateur reconnu ?};
    B -->|Oui| C[Bonjour M/Mme xxxxx,<br>Voulez vous déposer une clé ou emprunter une ?]
    C -->|Déposer| N{L'utilisateur possède une ou plusieurs clés ?}
    N -->|Oui| G[Scannez la clé yyyy];
    G --> O{La clé scanné est bien la clé yyyy ?}
    O -->|Non| P[Mauvaise clé];
    P --> G;
    O --> Q{L'utilisateur possède d'autres clés ?};
    Q -->|Oui| G;
    Q -->|Non| H{Ouverture de la porte};
    N -->|Non| D[vous n'avez aucune réservation dans <br>la prochaine heure];
    D --> A;
    H --> I[Veuillez penser à refermer la porte];
    I --> J{Contacteur de porte fermé ?};
    J -->|Oui| K[Bonne journée M/Mme xxxxxx];
    J -->|Non| L{boucle<20s ?};
    L -->|Oui| I;
    L -->|Non| M{Alarme de porte non fermée};
    M --> I;
    K --> A;
    B ---->|Non| E[Votre badge n'est pas reconnu];
    E --> A;
    C -->|Récupérer| S{L'utilisateur a-til une réservation<br> dans la prochaine heure ?};
    S -->|Oui| T{Ouverture de porte};
    T --> U{...};
```