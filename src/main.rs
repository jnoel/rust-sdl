use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::mouse::MouseButton;
use std::time::Duration;
use sdl2::rect::Rect;
use sdl2::render::{Texture, TextureCreator};
use sdl2::video::WindowContext;
use sdl2::image::{LoadTexture, InitFlag};

static SCREEN_WIDTH: u32 = 800;
static SCREEN_HEIGHT: u32 = 600;

fn left_clic(x: i32, y: i32) -> (bool, String) {
    println!("Left clic detected => x:{}, y:{}", x, y);
    if x>230 && x<282 && y>408 && y<443 {
        println!("Vous avez choisi la pillule rouge !");
        (true, "Vous avez choisi la pillule rouge !".to_string())
    } else if x>491 && x<552 && y>403 && y<442 {
        println!("Vous avez choisi la pillule bleue !");
        (true, "Pff, petit joueur !".to_string())
    } else {
        (true, "Bonjour M. Anderson !".to_string())
    }
    
}

fn create_texture_from_text<'a>(texture_creator: &'a 
    TextureCreator<WindowContext>,
    font: &sdl2::ttf::Font,
    text: &str,
    r: u8, g: u8, b: u8,
    ) -> Option<Texture<'a>> {
        if let Ok(surface) = font.render(text)
                .blended(Color::RGB(r, g, b)) {
            texture_creator.create_texture_from_surface(&surface).ok()
        } else {
            None
        }

}

fn create_background_color(i: i16, mut sens: i16) -> (u8, u8, u8, i16) {
    match i {
        0 => { sens = 1 },
        255 => { sens = -1 },
        _ => {}
    }
    //println!("i={}, sens={}", i, sens);
    let red = i as u8;
    let green = 128;
    let blue = (255 - i) as u8;
    return (red, green, blue, sens);
}
 
pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let ttf_context = sdl2::ttf::init().expect("SDL TTF initialization
        failed");
    let font = ttf_context.load_font("resources/BubblegumSans-Regular.otf", 128).expect("
        Couldn't load the font");
    sdl2::image::init(InitFlag::PNG | InitFlag::JPG).expect("Couldn't initialize
        image context");

    let video_subsystem = sdl_context.video().unwrap();
 
    let window = video_subsystem
        .window("rust-sdl2 demo", SCREEN_WIDTH, SCREEN_HEIGHT)
        .position_centered()
        .build()
        .unwrap();
 
    let mut canvas = window.into_canvas().build().unwrap();

    canvas.set_draw_color(Color::RGB(0, 255, 255));
    canvas.clear();

    let texture_creator: TextureCreator<_> =  
       canvas.texture_creator();

    let text = "Bonjour M. Anderson !";
    let mut rendered_text = create_texture_from_text(&texture_creator,
        &font, text, 255, 255, 255).expect("Cannot render text");
    let mut text_length = text.len();

    let image_texture = texture_creator.load_texture("resources/red-pill-blue-pill.png")
        .expect("Couldn't load image");
    
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut i = 0;
    let mut sens: i16 = 1;
    'running: loop {
        i = i + sens;

        let (red, green, blue, new_sens) = create_background_color(i, sens);
        sens = new_sens;

        canvas.set_draw_color(Color::RGB(red, green, blue));
        canvas.clear();

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    println!("bye bye !");
                    break 'running
                },
                Event::MouseButtonDown { x, y, mouse_btn: MouseButton::Left, .. } => {
                    let (change, text) = left_clic(x, y);
                    if change {
                        rendered_text = create_texture_from_text(&texture_creator,
                            &font, &text, 255, 255, 255).expect("Cannot render text");
                        text_length = text.len();
                    }
                }
                _ => {}
            }
        }
        // The rest of the game loop goes here...

        canvas.copy(&rendered_text, 
            None, 
            Some(Rect::new((SCREEN_WIDTH/2 - (text_length*6) as u32) as i32, 50, (text_length*12) as u32, 60)))
            .expect("Couldn't copy text");

        canvas.copy(&image_texture, 
            None, 
            Some(Rect::new(0, 200, 800, 400)))
            .expect("Render failed");

        canvas.present();

        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }
}